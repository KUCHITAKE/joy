﻿// -*- C++ -*-
/*!
 * @file  joyTest.cpp
 * @brief linux joystick component
 * @date $Date$
 *
 * $Id$
 */

#include "joyTest.h"

// Module specification
// <rtc-template block="module_spec">
static const char* joy_spec[] =
  {
    "implementation_id", "joyTest",
    "type_name",         "joyTest",
    "description",       "linux joystick component",
    "version",           "1.0.0",
    "vendor",            "NUT Kimuralab Keitaro Takeuchi",
    "category",          "Controller",
    "activity_type",     "EVENTDRIVEN",
    "kind",              "DataFlowComponent",
    "max_instance",      "1",
    "language",          "C++",
    "lang_type",         "compile",
    ""
  };
// </rtc-template>

/*!
 * @brief constructor
 * @param manager Maneger Object
 */
joyTest::joyTest(RTC::Manager* manager)
    // <rtc-template block="initializer">
  : RTC::DataFlowComponentBase(manager),
    m_axesOut("axes", m_axes),
    m_buttonsOut("buttons", m_buttons)

    // </rtc-template>
{
}

/*!
 * @brief destructor
 */
joyTest::~joyTest()
{
}



RTC::ReturnCode_t joyTest::onInitialize()
{
  // Registration: InPort/OutPort/Service
  // <rtc-template block="registration">
  // Set InPort buffers
  addInPort("axes", m_axesIn);
  addInPort("buttons", m_buttonsIn);

  // Set OutPort buffer

  // Set service provider to Ports

  // Set service consumers to Ports

  // Set CORBA Service Ports

  // </rtc-template>

  // <rtc-template block="bind_config">
  // </rtc-template>

  return RTC::RTC_OK;
}

/*
RTC::ReturnCode_t joyTest::onFinalize()
{
  return RTC::RTC_OK;
}
*/


RTC::ReturnCode_t joyTest::onStartup(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}


RTC::ReturnCode_t joyTest::onShutdown(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}


RTC::ReturnCode_t joyTest::onActivated(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}


RTC::ReturnCode_t joyTest::onDeactivated(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}


RTC::ReturnCode_t joyTest::onExecute(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}

/*
RTC::ReturnCode_t joyTest::onAborting(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}
*/

/*
RTC::ReturnCode_t joyTest::onError(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}
*/


RTC::ReturnCode_t joyTest::onReset(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}

/*
RTC::ReturnCode_t joyTest::onStateUpdate(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}
*/

/*
RTC::ReturnCode_t joyTest::onRateChanged(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}
*/



extern "C"
{

  void joyTestInit(RTC::Manager* manager)
  {
    coil::Properties profile(joy_spec);
    manager->registerFactory(profile,
                             RTC::Create<joyTest>,
                             RTC::Delete<joyTest>);
  }

};


