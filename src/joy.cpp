﻿// -*- C++ -*-
/*!
 * @file  joy.cpp
 * @brief linux joystick component
 * @date $Date$
 *
 * $Id$
 */

#include "joy.h"
#include <fcntl.h>
#include <unistd.h>
#include <linux/input.h>
#include <linux/joystick.h>

// Module specification
// <rtc-template block="module_spec">
static const char* joy_spec[] =
  {
    "implementation_id", "joy",
    "type_name",         "joy",
    "description",       "linux joystick component",
    "version",           "1.0.0",
    "vendor",            "NUT Kimuralab Keitaro Takeuchi",
    "category",          "Controller",
    "activity_type",     "EVENTDRIVEN",
    "kind",              "DataFlowComponent",
    "max_instance",      "1",
    "language",          "C++",
    "lang_type",         "compile",
    ""
  };
// </rtc-template>

/*!
 * @brief constructor
 * @param manager Maneger Object
 */
joy::joy(RTC::Manager* manager)
    // <rtc-template block="initializer">
  : RTC::DataFlowComponentBase(manager),
    m_axesOut("axes", m_axes),
    m_buttonsOut("buttons", m_buttons)

    // </rtc-template>
{
}

/*!
 * @brief destructor
 */
joy::~joy()
{
}



RTC::ReturnCode_t joy::onInitialize()
{
  // Registration: InPort/OutPort/Service
  // <rtc-template block="registration">
  // Set InPort buffers

  // Set OutPort buffer
  fd = -1;
  addOutPort("axes", m_axesOut);
  addOutPort("buttons", m_buttonsOut);

  // Set service provider to Ports

  // Set service consumers to Ports

  // Set CORBA Service Ports

  // </rtc-template>

  // <rtc-template block="bind_config">
  // </rtc-template>

  return RTC::RTC_OK;
}

/*
RTC::ReturnCode_t joy::onFinalize()
{
  return RTC::RTC_OK;
}
*/


RTC::ReturnCode_t joy::onStartup(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}


RTC::ReturnCode_t joy::onShutdown(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}


RTC::ReturnCode_t joy::onActivated(RTC::UniqueId ec_id)
{
  fd = open("/dev/input/js0", O_RDONLY);
  if(fd < 0) {
    std::cerr <<"Failed to open" << std::endl;
    return RTC::RTC_ERROR;
  }

  int axisNum{0};
  int buttonNum{0};
  char name[80];
  ioctl(fd, JSIOCGAXES, &axisNum);
  ioctl(fd, JSIOCGBUTTONS, &buttonNum);
  ioctl(fd, JSIOCGNAME(80), &name);
  m_axes.data.length(axisNum);
  m_buttons.data.length(buttonNum);

  std::cout << "Joystick: " << name << std::endl 
    << "Axes: " << axisNum << std::endl
    << "Buttons: " << buttonNum << std::endl;

  fcntl(fd, F_SETFL, O_NONBLOCK);

  return RTC::RTC_OK;
}


RTC::ReturnCode_t joy::onDeactivated(RTC::UniqueId ec_id)
{
  close(fd);
  return RTC::RTC_OK;
}


RTC::ReturnCode_t joy::onExecute(RTC::UniqueId ec_id)
{
  js_event js;

  if (read(fd, &js, sizeof(js_event))) {

  switch (js.type & ~JS_EVENT_INIT) {
    case JS_EVENT_AXIS:
      m_axes.data[js.number] = js.value / 32767.0;
      m_axesOut.write();
      break;

    case JS_EVENT_BUTTON:
      m_buttons.data[js.number] = (js.value != 0);
      m_buttonsOut.write();
      break;
  }


  // std::cout << "=====================" << std::endl;
  // for(unsigned int i{0}; i < m_axes.data.length(); i++) {
  //   std::cout << m_axes.data[i] << " ";
  // }
  // std::cout << std::endl;

  // for(unsigned int i{0}; i < m_buttons.data.length(); i++) {
  //   std::cout << m_buttons.data[i] << " ";
  // }
  // std::cout << std::endl;

  return RTC::RTC_OK;
  } else {
    return RTC::RTC_ERROR;
  }
}

/*
RTC::ReturnCode_t joy::onAborting(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}
*/

/*
RTC::ReturnCode_t joy::onError(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}
*/


RTC::ReturnCode_t joy::onReset(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}

/*
RTC::ReturnCode_t joy::onStateUpdate(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}
*/

/*
RTC::ReturnCode_t joy::onRateChanged(RTC::UniqueId ec_id)
{
  return RTC::RTC_OK;
}
*/



extern "C"
{

  void joyInit(RTC::Manager* manager)
  {
    coil::Properties profile(joy_spec);
    manager->registerFactory(profile,
                             RTC::Create<joy>,
                             RTC::Delete<joy>);
  }

};


